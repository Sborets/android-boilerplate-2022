package com.s808.testassignmentboilerplate.network.di

import com.s808.testassignmentboilerplate.network.retrofit.RetrofitProvider
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
object NetworkModule {

    @Provides
    fun getRetrofitProvider(): RetrofitProvider = RetrofitProvider()

    @Provides
    fun provideRetrofit(retrofitProvider: RetrofitProvider): Retrofit =
        retrofitProvider.getRetrofit()
}