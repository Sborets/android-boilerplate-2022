package com.s808.testassignmentboilerplate.network.service.news

import com.google.gson.annotations.SerializedName

data class NewsDTO(
    @SerializedName("author")
    val author: String,
    @SerializedName("content")
    val content: String,
    @SerializedName("imageUrl")
    val imageUrl: String,
    @SerializedName("title")
    val title: String,
)

data class NewsApiResponce(
    @SerializedName("category")
    val category: String,
    @SerializedName("data")
    val data: List<NewsDTO>
)