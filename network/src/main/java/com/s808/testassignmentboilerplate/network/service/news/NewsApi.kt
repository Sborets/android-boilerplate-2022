package com.s808.testassignmentboilerplate.network.service.news

import retrofit2.http.GET
import retrofit2.http.Url

interface NewsApi {

    companion object {
        const val baseUrl = "https://inshorts.deta.dev/"
    }

    @GET
    suspend fun getUsers(@Url url: String): NewsApiResponce
}
