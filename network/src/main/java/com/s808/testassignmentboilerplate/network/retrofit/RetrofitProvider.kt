package com.s808.testassignmentboilerplate.network.retrofit

import androidx.annotation.VisibleForTesting
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitProvider {

    /*As Retrofit instances are quite expensive we want to have only one in our project.
    Therefore we will have different base URLs for different APIs. That's why we shall
    pass URLs as arguments of Retrofit APIs methods using [@Url] notation. */
    @VisibleForTesting(otherwise = VisibleForTesting.PRIVATE)
    internal val baseUrl = "https://api.com"

    private val retrofitInstance: Retrofit by lazy {
        Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    fun getRetrofit(): Retrofit = retrofitInstance

    inline fun <reified T>getNetworkApi(): T {
        val retro = getRetrofit()
        return retro.create(T::class.java)
    }
}