package com.s808.testassignmentboilerplate.network

import com.s808.testassignmentboilerplate.network.retrofit.RetrofitProvider
import org.junit.Test
import retrofit2.http.GET

class RetrofitProviderTest {

    @Test
    fun `RetrofitProvider returns a real instance`() {
        val provider = RetrofitProvider()
        val retro = provider.getRetrofit()
        assert(retro.baseUrl().url().toString() == "${provider.baseUrl}/")
    }

    @Test
    fun `RetrofitProvider-getNetworkApi returns a proper type`() {
        val provider = RetrofitProvider()
        val api: Any = provider.getNetworkApi<TestingApi>()
        assert(api is TestingApi)
    }

    internal interface TestingApi {
        @GET
        suspend fun getSomething(): Nothing
    }
}