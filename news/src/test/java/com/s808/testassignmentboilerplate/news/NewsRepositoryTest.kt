package com.s808.testassignmentboilerplate.news

import com.s808.testassignmentboilerplate.network.service.news.NewsApi
import com.s808.testassignmentboilerplate.network.service.news.NewsApiResponce
import com.s808.testassignmentboilerplate.network.service.news.NewsDTO
import com.s808.testassignmentboilerplate.news.repo.NewsRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever


@Suppress("EXPERIMENTAL_IS_NOT_ENABLED")
@RunWith(MockitoJUnitRunner::class)
class NewsRepositoryTest {

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun `NewsRepository-getItems does mapping work properly`() = runTest {

        val response = NewsApiResponce(
            category = "MOCK",
            data = listOf(
                NewsDTO("MOCK", "MOCK", "MOCK", "MOCK")
            )
        )

        val newsApi: NewsApi = mock()
        whenever(newsApi.getUsers(any())).thenReturn(response)

        val repo = NewsRepository(newsApi)
        val items = repo.getItems()

        assert(items.size == 1 && items[0].author == "MOCK")
    }
}