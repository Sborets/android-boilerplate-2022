package com.s808.testassignmentboilerplate.news.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.s808.testassignmentboilerplate.core.navigation.MainNavigation
import com.s808.testassignmentboilerplate.news.repo.NewsItem
import com.s808.testassignmentboilerplate.news.ui.adapter.NewsListAdapter
import com.s808.testassignmentboilerplate.news.ui.state.NewsListScreenState
import com.s808.testassignmentboilerplate.news.ui.viewmodel.NewsViewModel
import com.s808.testassignmentboilerplate.resources.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NewsListFragment : Fragment() {

    private val viewModel: NewsViewModel by activityViewModels()
    private var recycler: RecyclerView? = null
    private var progressBar: ProgressBar? = null
    private var adapter: NewsListAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_news_list_v, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler = activity?.findViewById(R.id.news_list_recycler)
        progressBar = activity?.findViewById(R.id.progress_bar)
        adapter = NewsListAdapter().apply {
            onItemClick = {
                openDetailsScreen(it)
            }
        }
    }

    override fun onResume() {
        super.onResume()

        recycler?.layoutManager = LinearLayoutManager(requireContext())
        recycler?.adapter = adapter

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel.state.collect { state ->
                when (state) {
                    is NewsListScreenState.Error -> {
                        recycler?.isVisible = false
                        progressBar?.isVisible = false
                    }
                    is NewsListScreenState.Loading -> {
                        recycler?.isVisible = false
                        progressBar?.isVisible = true
                    }
                    is NewsListScreenState.Success -> {
                        recycler?.isVisible = true
                        progressBar?.isVisible = false
                        adapter?.addData(state.itemsList)
                    }
                    is NewsListScreenState.Idle -> {
                        //This is the initial state of the screen.
                        //Do something here if needed.
                    }
                }
            }
        }

        viewModel.requestNewsList()
    }

    private fun openDetailsScreen(item: NewsItem) {
        val navigation = requireActivity() as MainNavigation
        navigation.navigateTo(MainNavigation.NewsDetailsV(item.imageUrl, item.title, item.content))
    }
}
