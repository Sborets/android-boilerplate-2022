package com.s808.testassignmentboilerplate.news.ui.adapter

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.s808.testassignmentboilerplate.core.graphics.Graphics
import com.s808.testassignmentboilerplate.resources.R
import com.s808.testassignmentboilerplate.news.repo.NewsItem

class NewsViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
    fun onBind(item: NewsItem, onItemClick: ((NewsItem) -> Unit)?) {

        val title = view.findViewById<TextView>(R.id.news_title)
        val subtitle = view.findViewById<TextView>(R.id.news_subtitle)
        val icon = view.findViewById<ImageView>(R.id.news_icon)

        title.text = item.title
        subtitle.text = item.content
        Graphics.renderImage(
            context = view.context,
            imageView = icon,
            url = item.imageUrl
        )

        itemView.setOnClickListener {
            onItemClick?.invoke(item)
        }
    }
}
