@file:Suppress("unused")

package com.s808.testassignmentboilerplate.news.ui.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.s808.testassignmentboilerplate.news.repo.NewsItem
import com.s808.testassignmentboilerplate.resources.R


class NewsListAdapter : RecyclerView.Adapter<NewsViewHolder>() {

    var onItemClick: ((NewsItem) -> Unit)? = null
    private val dataSet: MutableList<NewsItem> = mutableListOf()

    @SuppressLint("NotifyDataSetChanged")
    fun addData(newData: List<NewsItem>) {
        dataSet.addAll(newData)
        notifyDataSetChanged() //TODO: Implement DiffUtil and partial list updates
    }

    fun resetData(newData: List<NewsItem>) {
        dataSet.clear()
        addData(newData)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder =
        NewsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false))

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.onBind(dataSet[position], onItemClick)
    }

    override fun getItemCount(): Int = dataSet.size
}
