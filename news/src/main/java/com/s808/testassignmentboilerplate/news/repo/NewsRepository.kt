package com.s808.testassignmentboilerplate.news.repo

import com.s808.testassignmentboilerplate.core.repo.RepoParams
import com.s808.testassignmentboilerplate.core.repo.Repository
import com.s808.testassignmentboilerplate.network.service.news.NewsApi


class NewsRepository(private val newsNetworkApi: NewsApi) : Repository<NewsItem> {

    override suspend fun getItems(repoParams: RepoParams?): List<NewsItem> =
        /*
        * On this level we map [NewsDTO] objects into [NewsItem].
        * This approach separates REST API data structure and our App Feature data structure.
        * In case something is changed on server side, we hopefully only need to update a small
        * separated part of code.
        * */
        newsNetworkApi.getUsers("${NewsApi.baseUrl}/news?category=science").data.map {
            NewsItem(
                author = it.author,
                content = it.content,
                imageUrl = it.imageUrl,
                title = it.title,
            )
        }

}
