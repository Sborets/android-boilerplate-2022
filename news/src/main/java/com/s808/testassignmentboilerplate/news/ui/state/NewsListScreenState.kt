package com.s808.testassignmentboilerplate.news.ui.state

import com.s808.testassignmentboilerplate.news.repo.NewsItem


sealed class NewsListScreenState {
    class Success(val itemsList: List<NewsItem>) : NewsListScreenState()
    class Loading() : NewsListScreenState()
    class Error(val message: String) : NewsListScreenState()
    class Idle() : NewsListScreenState()
}
