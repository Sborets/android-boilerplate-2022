package com.s808.testassignmentboilerplate.news.model

import com.s808.testassignmentboilerplate.core.result.OperationResult
import com.s808.testassignmentboilerplate.network.retrofit.RetrofitProvider
import com.s808.testassignmentboilerplate.network.service.news.NewsApi
import com.s808.testassignmentboilerplate.news.repo.NewsItem
import com.s808.testassignmentboilerplate.news.repo.NewsRepository

class NewsModel() {

    //TODO: To be moved to constructor
    private val newsRepository: NewsRepository by lazy {
        val retrofitProvider = RetrofitProvider()
        NewsRepository(retrofitProvider.getNetworkApi<NewsApi>())
    }
    suspend fun getNewsList(): OperationResult<List<NewsItem>> {
        return try {
            OperationResult.success(data = newsRepository.getItems())
        } catch (exception: Exception) {
            OperationResult.error(data = null, message = exception.message ?: "Error Occurred!")
        }
    }
}