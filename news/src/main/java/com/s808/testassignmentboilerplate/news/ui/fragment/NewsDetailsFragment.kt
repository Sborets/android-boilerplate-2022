package com.s808.testassignmentboilerplate.news.ui.fragment

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.s808.testassignmentboilerplate.core.graphics.Graphics
import com.s808.testassignmentboilerplate.resources.R
import kotlinx.parcelize.Parcelize

class NewsDetailsFragment : Fragment() {

    companion object {
        private const val key = "params"
        fun getInstance(params: FragmentParams): NewsDetailsFragment =
            NewsDetailsFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(key, params)
                }
            }
    }

    private var icon: ImageView? = null
    private var back: ImageView? = null
    private var title: TextView? = null
    private var newsText: TextView? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_news_details_v, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        icon = activity?.findViewById(R.id.news_picture)
        back = activity?.findViewById(R.id.news_details_back_btn)
        title = activity?.findViewById(R.id.news_details_title)
        newsText = activity?.findViewById(R.id.news_text)
    }

    override fun onResume() {
        super.onResume()

        val params: FragmentParams = arguments?.getParcelable(key)
            ?: throw IllegalStateException("No params passed into fragment. Create fragment instance via getInstance() function")

        icon?.let {
            Graphics.renderImage(requireContext(), it, params.iconUrl)
        }

        title?.text = params.title
        newsText?.text = params.text

        back?.setOnClickListener {
            requireActivity().onBackPressed()
        }
    }

    @Parcelize
    data class FragmentParams(
        val iconUrl: String,
        val title: String,
        val text: String
    ) : Parcelable
}