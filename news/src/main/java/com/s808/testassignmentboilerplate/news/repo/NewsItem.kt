package com.s808.testassignmentboilerplate.news.repo

data class NewsItem (
    val author: String,
    val content: String,
    val imageUrl: String,
    val title: String,
)
