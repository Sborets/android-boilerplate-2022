package com.s808.testassignmentboilerplate.news.ui.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.s808.testassignmentboilerplate.core.result.OperationStatus.*
import com.s808.testassignmentboilerplate.news.model.NewsModel
import com.s808.testassignmentboilerplate.news.ui.state.NewsListScreenState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch


class NewsViewModel : ViewModel() {

    //TODO: Move to constructor
    private val newsModel = NewsModel()

    private val _state = MutableStateFlow<NewsListScreenState>(NewsListScreenState.Idle())
    val state: StateFlow<NewsListScreenState> = _state

    fun requestNewsList() {
        _state.value = NewsListScreenState.Loading()
        viewModelScope.launch(Dispatchers.IO) {
            val result = newsModel.getNewsList()
            when (result.status) {
                SUCCESS -> {
                    val news = result.data
                    news?.let {
                        _state.value = NewsListScreenState.Success(news)
                    }
                }
                else -> {
                    Log.d(
                        "NewsViewModel",
                        "News responce was unsuccessful.\n ${result.status}\n${result.message}"
                    )
                    //TODO: Handle unhappy cases
                }
            }

        }
    }
}
