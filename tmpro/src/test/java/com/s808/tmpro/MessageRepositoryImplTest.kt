package com.s808.tmpro

import android.content.SharedPreferences
import com.s808.tmpro.data.MessageRepositoryImpl
import org.junit.Test

import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.*

@Suppress("EXPERIMENTAL_IS_NOT_ENABLED")
@RunWith(MockitoJUnitRunner::class)
class MessageRepositoryImplTest {

    private val sharedPreferences: SharedPreferences = mock()
    private val editor: SharedPreferences.Editor = mock()
    private val repo = MessageRepositoryImpl(sharedPreferences)

    @Test
    fun `delete() calls SP apply()`() {
        whenever(sharedPreferences.edit()).thenReturn(editor)
        repo.deleteMessage()
        verify(editor, times(1)).apply()
    }

    @Test
    fun `saveMessage() calls SP apply()`() {
        whenever(sharedPreferences.edit()).thenReturn(editor)
        whenever(editor.putString(any(), any())).thenReturn(editor)
        repo.saveMessage("BOO")
        verify(editor, times(1)).apply()
    }

    @Test
    fun `saveMessage() puts string to SP`() {
        whenever(sharedPreferences.edit()).thenReturn(editor)
        whenever(editor.putString(any(), any())).thenReturn(editor)
        repo.saveMessage("BOO")
        verify(editor).putString("TmProMessage", "BOO")
    }
}
