package com.s808.tmpro.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.s808.tmpro.data.MessageRepository
import com.s808.tmpro.presentation.screenstate.DefaultScreenState
import com.s808.tmpro.presentation.screenstate.MessageScreenState
import com.s808.tmpro.presentation.screenstate.TmProScreenState
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class TmProViewModel(private val messageRepository: MessageRepository) : ViewModel() {
    private val _state = MutableStateFlow<TmProScreenState>(DefaultScreenState)
    val state: StateFlow<TmProScreenState> = _state

    fun onResume() {
        viewModelScope.launch {
            val message = messageRepository.getMessage()
            if (message.isNullOrEmpty()) {
                _state.value = DefaultScreenState
            } else {
                _state.value = MessageScreenState("*".repeat(message.length), false)
            }
        }

    }

    fun deleteMessage() {
        viewModelScope.launch {
            messageRepository.deleteMessage()
            _state.value = DefaultScreenState
        }
    }

    fun persistMessage(message: String) {
        viewModelScope.launch {
            messageRepository.saveMessage(message)
            _state.value = MessageScreenState("*".repeat(message.length), false)
        }

    }

    fun showMessage() {
        viewModelScope.launch {
            messageRepository.getMessage()?.let {
                _state.value = MessageScreenState(it, true)
            }
        }
    }

    fun hideMessage() {
        viewModelScope.launch {
            val size = messageRepository.getMessageSize()
            _state.value = MessageScreenState("*".repeat(size), false)
        }
    }
}
