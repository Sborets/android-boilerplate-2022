package com.s808.tmpro.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import com.google.android.material.button.MaterialButton
import com.s808.tmpro.R

class TmProInputFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_tmpro_input, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val input: EditText? = activity?.findViewById(R.id.message_input)
        val submit: MaterialButton? = activity?.findViewById(R.id.submit_message)

        submit?.setOnClickListener {
            val message = input?.text?.toString()
            setFragmentResult(TmProFragment.requestKey, bundleOf(TmProFragment.bundleKey to message))
            requireActivity().onBackPressed()
        }

        //TODO: It would be nice to figure out if there is a possible limit for message length.
        //If it is, we could measure an input programmaticaly and notify user that the limit is exceeded.
    }
}
