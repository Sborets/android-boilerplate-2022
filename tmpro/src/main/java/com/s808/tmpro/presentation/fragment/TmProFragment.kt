package com.s808.tmpro.presentation.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResultListener
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import com.google.android.material.button.MaterialButton
import com.s808.testassignmentboilerplate.core.navigation.MainNavigation
import com.s808.tmpro.R
import com.s808.tmpro.presentation.screenstate.DefaultScreenState
import com.s808.tmpro.presentation.screenstate.MessageScreenState
import com.s808.tmpro.presentation.viewmodel.TmProViewModel
import com.s808.tmpro.presentation.viewmodel.TmProViewModelFactory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class TmProFragment : Fragment(), View.OnClickListener {

    companion object {
        const val requestKey = "requestKey"
        const val bundleKey = "bundleKey"
    }

    private var viewModel: TmProViewModel? = null
    private var title: TextView? = null
    private var messageText: TextView? = null
    private var addTextButton: MaterialButton? = null
    private var deleteTextButton: MaterialButton? = null
    private var showTextButton: MaterialButton? = null
    private var hideTextButton: MaterialButton? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_tmpro, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val factory = TmProViewModelFactory(requireContext())
        viewModel = ViewModelProvider(this, factory)[TmProViewModel::class.java]

        setUpViews()

        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.Main) {
            viewModel?.state?.collect { state ->
                when(state) {
                    is DefaultScreenState -> {
                        title?.text = getString(R.string.no_message)
                        messageText?.text = ""
                        addTextButton?.isVisible = true
                        deleteTextButton?.isVisible = false
                        showTextButton?.isVisible = false
                        hideTextButton?.isVisible = false
                    }
                    is MessageScreenState -> {
                        title?.text = getString(R.string.message_exist)
                        messageText?.text = state.message
                        addTextButton?.isVisible = false
                        deleteTextButton?.isVisible = true
                        showTextButton?.isVisible = !state.messageIsVisible
                        hideTextButton?.isVisible = state.messageIsVisible
                    }
                }
            }
        }

        setFragmentResultListener(requestKey) { key, bundle ->
            if(key == requestKey) {
                val message = bundle.getString(bundleKey)
                if (!message.isNullOrEmpty()) {
                    viewModel?.persistMessage(message)
                }
            }
        }
    }

    override fun onClick(p0: View?) {
        when(p0?.id) {
            R.id.add_message -> {
                val navigation = requireActivity() as? MainNavigation
                navigation?.navigateTo(MainNavigation.TmProInput())
            }
            R.id.delete_message -> viewModel?.deleteMessage()
            R.id.show_message -> viewModel?.showMessage()
            R.id.hide_message -> viewModel?.hideMessage()
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel?.onResume()
    }

    private fun setUpViews() {
        activity?.let {
            val listener: View.OnClickListener = this
            title = it.findViewById(R.id.title)
            messageText = it.findViewById(R.id.message_text)
            addTextButton = it.findViewById<MaterialButton?>(R.id.add_message).apply { setOnClickListener(listener) }
            deleteTextButton = it.findViewById<MaterialButton?>(R.id.delete_message).apply { setOnClickListener(listener) }
            showTextButton = it.findViewById<MaterialButton?>(R.id.show_message).apply { setOnClickListener(listener) }
            hideTextButton = it.findViewById<MaterialButton?>(R.id.hide_message).apply { setOnClickListener(listener) }
        }
    }
}
