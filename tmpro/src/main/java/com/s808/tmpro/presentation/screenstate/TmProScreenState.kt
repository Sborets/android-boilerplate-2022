package com.s808.tmpro.presentation.screenstate

sealed class TmProScreenState
object DefaultScreenState : TmProScreenState()
class MessageScreenState(val message: String, val messageIsVisible: Boolean) : TmProScreenState()