package com.s808.tmpro.presentation.viewmodel

import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import com.s808.tmpro.R
import com.s808.tmpro.data.MessageRepository
import com.s808.tmpro.data.MessageRepositoryImpl

class TmProViewModelFactory(private val context: Context) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {

        val sharedPrefsFile: String = context.getString(R.string.shared_prefs_file)
        val mainKeyAlias = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)

        val sharedPreferences: SharedPreferences = EncryptedSharedPreferences.create(
            sharedPrefsFile,
            mainKeyAlias,
            context,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )

        val repo: MessageRepository = MessageRepositoryImpl(sharedPreferences)

        return if (modelClass.isAssignableFrom(TmProViewModel::class.java)) {
            TmProViewModel(repo) as T
        } else {
            throw IllegalArgumentException("ViewModel Not Found")
        }
    }
}
