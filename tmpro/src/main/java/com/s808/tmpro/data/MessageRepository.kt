package com.s808.tmpro.data

interface MessageRepository {
    fun getMessage(): String?
    fun getMessageSize(): Int
    fun deleteMessage()
    fun saveMessage(message: String)
}