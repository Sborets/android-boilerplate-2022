package com.s808.tmpro.data

import android.content.SharedPreferences

class MessageRepositoryImpl(private val sharedPreferences: SharedPreferences) : MessageRepository {

    private val messageKey = "TmProMessage"

    override fun saveMessage(message: String) {
        with (sharedPreferences.edit()) {
            putString(messageKey, message)
            apply()
        }
    }

    override fun getMessage(): String? = sharedPreferences.getString(messageKey, null)

    override fun getMessageSize(): Int = sharedPreferences.getString(messageKey, null)?.length?: 0

    override fun deleteMessage() {
        with (sharedPreferences.edit()) {
            putString(messageKey, null)
            apply()
        }
    }
}
