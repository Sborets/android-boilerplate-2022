package com.s808.testassignmentboilerplate.main.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.s808.testassignmentboilerplate.core.navigation.MainNavigation
import com.s808.testassignmentboilerplate.core.navigation.MainNavigation.*
import com.s808.testassignmentboilerplate.core.navigation.add
import com.s808.testassignmentboilerplate.core.navigation.replace
import com.s808.testassignmentboilerplate.news.ui.fragment.NewsDetailsFragment
import com.s808.testassignmentboilerplate.news.ui.fragment.NewsListFragment
import com.s808.testassignmentboilerplate.resources.R
import com.s808.tmpro.presentation.fragment.TmProFragment
import com.s808.tmpro.presentation.fragment.TmProInputFragment


class MainActivity : AppCompatActivity(), MainNavigation {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        showTmProFragment()
    }

    override fun navigateTo(screen: Screen) {
        when (screen) {
            is NewsDetailsV -> showNewsDetailsFragment(screen)
            is NewsListV -> showNewsListFragment()
            is TmPro -> showTmProFragment()
            is TmProInput -> showTmProInputFragment()
        }
    }

    private fun showNewsListFragment() {
        NewsListFragment().apply {
            replace(supportFragmentManager, R.id.main_container, false)
        }
    }

    private fun showTmProFragment() {
        TmProFragment().apply {
            replace(supportFragmentManager, R.id.main_container, false)
        }
    }

    private fun showTmProInputFragment() {
        TmProInputFragment().apply {
            add(supportFragmentManager, R.id.main_container, true)
        }
    }

    private fun showNewsDetailsFragment(screen: NewsDetailsV) {
        NewsDetailsFragment.getInstance(
            NewsDetailsFragment.FragmentParams(screen.iconUrl, screen.title, screen.text)
        ).apply {
            replace(supportFragmentManager, R.id.main_container, true)
        }
    }
}
