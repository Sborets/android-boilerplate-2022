package com.s808.testassignmentboilerplate.core.repo

abstract class RepoParams

inline fun <reified T> RepoParams.throwIllegalTypeException() {
    throw IllegalArgumentException("Illegal type was used. Argument must be a type of ${T::class.java.simpleName}")
}
