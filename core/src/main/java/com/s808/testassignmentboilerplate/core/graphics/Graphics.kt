package com.s808.testassignmentboilerplate.core.graphics

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide

//TODO: Refactor to interface and provide with dagger
object Graphics {

    fun renderImage(context: Context, imageView: ImageView, url: String, placeholder: Int? = null) {
        Glide.with(context).load(url).apply {
            if (placeholder != null) {
                placeholder(placeholder)
            }
        }.into(imageView)
    }
}

fun ImageView?.renderWebImage(context: Context, url: String?, placeholder: Int? = null) {
    this?.let { view ->
        url?.let { imageUrl ->
            Graphics.renderImage(context, view, imageUrl, placeholder)
        }
    }
}
