package com.s808.testassignmentboilerplate.core.navigation

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager

/**
 * Commits 'replace' fragment transaction using given FragmentManager and container id
 * [addToBackStack] is used to add fragment to back stack
 */
fun Fragment.replace(fm: FragmentManager, container: Int, addToBackStack: Boolean = true) {
    val transaction = fm.beginTransaction().replace(container, this)
    if (addToBackStack) {
        transaction.addToBackStack(this.javaClass.simpleName)
    }
    transaction.commit()
}

/**
 * Commits 'add' fragment transaction using given FragmentManager and container id
 * [addToBackStack] is used to add fragment to back stack
 */
fun Fragment.add(fm: FragmentManager, container: Int, addToBackStack: Boolean = true) {
    val transaction = fm.beginTransaction().add(container, this)
    if (addToBackStack) {
        transaction.addToBackStack(this.javaClass.simpleName)
    }
    transaction.commit()
}