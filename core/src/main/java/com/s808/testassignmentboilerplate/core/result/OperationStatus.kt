package com.s808.testassignmentboilerplate.core.result

enum class OperationStatus {
    SUCCESS,
    ERROR,
    LOADING
}
