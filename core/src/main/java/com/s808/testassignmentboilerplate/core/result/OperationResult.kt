package com.s808.testassignmentboilerplate.core.result

import com.s808.testassignmentboilerplate.core.result.OperationStatus.*

/**
 * This class is a universal wrapper to be used mostly with coroutines.
 * It is the way to perform an operation which has a potential to throw an exception.
 * Using this class we handle an exception as a specific result of operation whith it's
 * own status and message.
 */
data class OperationResult<out T>(val status: OperationStatus, val data: T?, val message: String?) {
    companion object {
        fun <T> success(data: T): OperationResult<T> = OperationResult(status = SUCCESS, data = data, message = null)
        fun <T> error(data: T?, message: String): OperationResult<T> = OperationResult(status = ERROR, data = data, message = message)
        @Suppress("unused")
        fun <T> loading(data: T?): OperationResult<T> = OperationResult(status = LOADING, data = data, message = null)
    }
}