package com.s808.testassignmentboilerplate.core.navigation

/**
 * Interface for main navigation
 */
interface MainNavigation {

    /**
     * This classed to be used as an argument to call navigation transaction in generalized fashion
     * [NewsListV] - News list, [View] implementation
     * [NewsDetailsV] - News list, [View] implementation
     */
    sealed class Screen
    class TmPro(): Screen()
    class TmProInput(): Screen()
    class NewsListV(): Screen()
    class NewsDetailsV(val iconUrl: String, val title: String, val text: String): Screen()

    /**
     * Navigates to desired screen
     */
    fun navigateTo(screen: Screen)
}
