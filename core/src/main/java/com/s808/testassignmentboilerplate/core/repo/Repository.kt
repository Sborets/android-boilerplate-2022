package com.s808.testassignmentboilerplate.core.repo

interface Repository<T> {
    suspend fun getItems(params: RepoParams? = null): List<T>
}